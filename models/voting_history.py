from .base import Base

from sqlalchemy import Column, Integer, String


class CFVID20VotingHistory(Base):
    __tablename__ = 'cfvid20_voting_history'

    date_voted_for = Column('DateVotedFor', String, primary_key=True)
    total_votes = Column('TotalVotes', Integer, nullable=True)
    vote_for_white = Column('VoteForWhite', Integer, nullable=True)
    vote_for_black = Column('VoteForBlack', Integer, nullable=True)

    def __init__(self, date_voted_for, total_votes, vote_for_white, vote_for_black):
        self.date_voted_for = date_voted_for
        self.total_votes = total_votes
        self.vote_for_white = vote_for_white
        self.vote_for_black = vote_for_black

    def __repr__(self):
        return "<CFVID20VotingHistory(date_voted_for='%s', total_votes='%s', " \
               "vote_for_white='%s', vote_for_black='%s')>" % \
               (self.date_voted_for, self.total_votes, self.vote_for_white, self.vote_for_black)


__all__ = (
    'CFVID20VotingHistory',
)

from models import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def create_db(name='sqlite:///./fun.db'):
    database_engine = create_engine(name)
    Session = sessionmaker(bind=database_engine)
    database_session = Session()

    Base.metadata.drop_all(bind=database_engine)
    Base.metadata.create_all(bind=database_engine)

    database_session.commit()


create_db()

import os

from dotenv import load_dotenv
from pathlib import Path
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from models import *

env_path = Path('./../.env')
load_dotenv(dotenv_path=env_path)


bau = os.getenv('DATABASE_URL')
engine = create_engine(bau, echo=True)

Session = sessionmaker(bind=engine)
database_session = Session()

voting = CFVID20VotingHistory(**{
    'date_voted_for': '2020-12-03',
    'total_votes': None,
    'vote_for_white': None,
    'vote_for_black': None
})
try:
    database_session.add(voting)
    database_session.commit()
except Exception as e:
    print(e.__str__())

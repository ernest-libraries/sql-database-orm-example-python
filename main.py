import os

from models import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session





# path = 'sqlite:///./history.db'
# engine = create_engine(path, echo=True)

# TODO: Check documentation
# M1: It works.
# database_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
# Base.query = database_session.query_property()

# M2: It also works
# Session = sessionmaker(bind=engine)
# database_session = Session()
#
# voting = CFVID20VotingHistory(**{
#     'date_voted_for': '2020-12-29',
#     'total_votes': None,
#     'vote_for_white': None,
#     'vote_for_black': None
# })
# try:
#     database_session.add(voting)
#     database_session.commit()
# except Exception as e:
#     print(e.__str__())
